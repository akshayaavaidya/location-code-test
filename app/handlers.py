import json
import pandas as pd
import urllib
from bs4 import BeautifulSoup
import numpy as np

def scrapper(link):
        html = urllib.request.urlopen(link)
        html = json.loads(BeautifulSoup(html.read(), 'lxml').find('p').getText())
        return html
    
def weather_scrap(name):
        html = scrapper ('https://www.metaweather.com/api/location/search/?query=' + name[0:3].lower())
        for cities in html:
            set1= set(cities['title'].split()).issubset(set(name.title().split()))
            set2= set(name.title().split()).issubset(set(cities['title'].split()))
            if set1 or set2:
                woeid= cities['woeid']
        weather_html= scrapper('https://www.metaweather.com/api/location/' + str(woeid))
        return weather_html
    
def csv_scrap():
        df = pd.read_csv('.\\data\\european_cities.csv')
        df['city']=df['city'].str.title()
        return df
    
def city_scorer(df,city_name):
        ''' Inorder to score each city based on their other attributes,
        every attribute is normalized to the range 0 to 1. 
        Cities with less crime rate are given more weightage (Inversely proportional).
        Hence 1- normalized crime rate. Same applies for average_hotel_cost field as well.
        Score is the mean of the normalized values of all the fields.
        '''
        df_norm= df[['population','bars','museums','public_transport','crime_rate','average_hotel_cost']]
        df_norm= (df_norm-df_norm.min())/(df_norm.max()-df_norm.min())
        df_norm['crime_rate']= 1- df_norm['crime_rate']
        df_norm['average_hotel_cost']= 1- df_norm['average_hotel_cost']
        df_norm['city']= df['city']
        df_norm['score']=(df_norm[['population','bars','museums','public_transport','crime_rate','average_hotel_cost']].mean(axis=1)) *10 
        score= float(list(df_norm[df_norm['city']== city_name.title()]['score'])[0])
        return score        

def ranker(compare_list):
        ''' Average of weather condition and city score is done to rank the cities '''
        compare_df=pd.DataFrame(compare_list,columns=['climate','score','city'])
        compare_df['ranker']= (compare_df['climate'] + compare_df['score'])/ 2
        compare_df['rank'] = compare_df['ranker'].rank(ascending=0)
        compare_df['city_name'] = np.where(compare_df['rank'] == 1, 'great place', np.where(compare_df['rank'] == 2, 'ok place', 'bad place'))
        compare_df=compare_df.sort_values(['rank'], ascending=1)        
        rank_list=[]
        rank_dict={}
        for index, row in compare_df.iterrows():
                rank_dict= {'city_name': row['city_name'] ,
                                   'city_rank': int(row['rank']),
                                   'city_score': float(row['ranker'])}
                rank_list.append(rank_dict)
        response= {'city_data': rank_list}
        return response