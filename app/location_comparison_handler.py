from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.handlers import *

class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        ''' Weather condition field is tagged to integers for each condition '''
        weather_tags= {'sn' : 1, 'sl' : 2, 'h' : 3, 't' : 4, 'hr' : 5, 'lr' : 6, 's' : 7, 'hc' : 8, 'lc' : 9, 'c' : 10}
        compare_list=[]
        for city in args[0].split(','):
             weather_html= weather_scrap(city)
             climate=weather_tags[weather_html['consolidated_weather'][-1]['weather_state_abbr']]
             df = csv_scrap()
             score= city_scorer(df,city)
             compare_list.append([climate, score, city])
             
        response= ranker(compare_list)
        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
