from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.handlers import *

class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()
              
    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
        city_name=str(args[0])
        df = csv_scrap()      
        weather_html= weather_scrap(city_name)
        city_score= city_scorer(df,city_name)


        response = {'city_name': city_name,
                    'current_temperature': weather_html['consolidated_weather'][-1]['the_temp'],
                    'current_weather_description': weather_html['consolidated_weather'][-1]['weather_state_name'],
                    'bars': int(list(df[df['city']== city_name.title()]['bars'])[0]),
                    'population': int(list(df[df['city']== city_name.title()]['population'])[0]),
                    'city_score': city_score}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
